DC = docker-compose -f docker-compose.yml
DATABASE_CONTAINER = db

.PHONY: help logs

.DEFAULT_GOAL := help

help: ## Display help
	@echo "Available targets:"
	@awk '/^[a-zA-Z_-]+:/ { \
		helpMessage = helpMessage; \
		n = split($$0, a, "##"); \
		if (n == 2) { \
			helpMessage = helpMessage "  " substr($$1, 1, length($$1)-1) "\t\t" a[2] "\n"; \
		} \
	} END {printf "%s", helpMessage}' $(MAKEFILE_LIST)

up: ## Start the docker containers
	$(DC) up -d --build

do: ## Stop the docker containers
	$(DC) down

logs: ## Show the docker logs
	$(DC) logs -f

ex: ## Enter the db container
	$(DC) exec $(DATABASE_CONTAINER) bash